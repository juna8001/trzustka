﻿using UnityEngine;

namespace Parallax
{
    public class ParallaxElement : MonoBehaviour
    {
        [SerializeField]
        private float _speed;
        [SerializeField]
        private MainParallax _parallax;
        [SerializeField]
        private Vector3 _constMove;
        [SerializeField]
        private Vector2 _parallaxBounds;

        private void Awake()
        {
            if (_parallax != null)
            {
                _parallax.ParallaxMoved += OnParallaxMoved;
            }
        }

        private void Update()
        {
            OnParallaxMoved(_constMove);
            if (transform.localPosition.x < _parallaxBounds.x)
            {
                transform.localPosition += Vector3.right * (_parallaxBounds.y - _parallaxBounds.x);
            }
            else if (transform.localPosition.x > _parallaxBounds.y)
            {
                transform.localPosition += Vector3.right * (_parallaxBounds.x - _parallaxBounds.y);
            }
        }

        private void OnDestroy()
        {
            if (_parallax != null)
            {
                _parallax.ParallaxMoved -= OnParallaxMoved;
            }
        }

        private void OnParallaxMoved(Vector3 move)
        {
            transform.Translate(move * _speed * Time.deltaTime);
        }
    }
}