﻿using System;
using UnityEngine;

namespace Parallax
{
    public class MainParallax : MonoBehaviour
    {
#if UNITY_EDITOR
        [SerializeField]
        [Header("For Debugging - don't use in build!")]
        private Vector3 _constMove;

        private void Update()
        {
            MoveParallax(_constMove);
        }
#endif

        //Reference to this:
        //For move use the real move speed of the character
        //Only the X axis is prepared
        public void MoveParallax(Vector3 move)
        {
            OnParallaxMoved(move);
        }

        public event Action<Vector3> ParallaxMoved;
        private void OnParallaxMoved(Vector3 move)
        {
            ParallaxMoved(move);
        }
    }
}