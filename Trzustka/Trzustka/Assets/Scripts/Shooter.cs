﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    [SerializeField] private float _delay;
    [SerializeField] private GameObject _prefab;
    [SerializeField] private Transform _emitPoint;

    private float _nextShootTime;
    private GroundChecker _groundChecker;
    private Attacker _attacker;

    protected void Start()
    {
        _groundChecker = GetComponentInChildren<GroundChecker>();
        _attacker = GetComponent<Attacker>();
    }
    
    public void TryShootToRight(float speed)
    {
        if (CanShoot())
        {
            ShootToRight(speed);
        }
    }
    
    public void ShootToRight(float speed)
    {
        Shoot(_emitPoint.right*speed);
    }

    public void TryShoot(Vector2 velocity)
    {
        if (CanShoot())
        {
            Shoot(velocity);
        }
    }
    
    public void Shoot(Vector2 velocity)
    {
        _nextShootTime = Time.time + _delay;
        foreach (var c in Instantiate(_prefab, _emitPoint.position, _emitPoint.rotation).GetComponentsInChildren<IShootable>())
        {
            c.OnShooted(this, velocity);
        }
    }

    public bool ShootTime()
    {
        return Time.time >= _nextShootTime;
    }

    public bool CanShoot()
    {
        if (_groundChecker && _groundChecker.OnGround) return false;
        
        return !_attacker.Active && ShootTime();
    }
}
