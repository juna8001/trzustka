﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attacker : MonoBehaviour
{
    [SerializeField] private GameObject _attack;
    [SerializeField] private float _attackTime = 1;
    [SerializeField] private float _activateAttackTime = 0.35f;
    [SerializeField] private float _disableAttackTime = 0.65f;
    [SerializeField] private Animator _animator;

    private GroundChecker _groundChecker;
    private Shooter _shooter;
    
    public bool Active { get; private set; }

    protected void Awake()
    {
        _groundChecker = GetComponentInChildren<GroundChecker>();
        _shooter = GetComponent<Shooter>();
    }
    
    public void TryAttack()
    {
        if(CanAttack()) Attack();
    }
    
    public void Attack()
    {
        _attackCoroutine = StartCoroutine(AttackCoroutine());
    }

    private bool CanAttack()
    {
        if (_groundChecker && !_groundChecker.OnGround) return false;
        if (_shooter && !_shooter.ShootTime()) return false;
        return _attackCoroutine == null;
    }

    private Coroutine _attackCoroutine;
    private IEnumerator AttackCoroutine()
    {
        Active = true;
        _animator.Play("Attack");
        yield return new WaitForSeconds(_activateAttackTime);
        if (!_groundChecker.OnGround)
        {
            Active = false;
            _attackCoroutine = null;
            yield break;
        }
        _attack.SetActive(true);
        yield return new WaitForSeconds(_disableAttackTime-_activateAttackTime);
        _attack.SetActive(false);
        yield return new WaitForSeconds(_attackTime-_disableAttackTime);
        Active = false;
        _attackCoroutine = null;
    }
}
