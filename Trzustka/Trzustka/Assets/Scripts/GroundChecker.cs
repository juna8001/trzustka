﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GroundChecker : MonoBehaviour
{
    public bool OnGround { get; private set; }
    private List<Collider2D> _grounds = new List<Collider2D>();
    private List<Vector2> _positions = new List<Vector2>();

    private Rigidbody2D _body;

    protected void Awake()
    {
        _body = GetComponentInParent<Rigidbody2D>();
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (!_grounds.Contains(other)) OnTriggerEnter2D(other);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.attachedRigidbody == _body) return;
        _grounds.Add(other);
        _positions.Add(other.transform.position);
        OnGround = true;
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        var i = _grounds.IndexOf(other);
        _grounds.RemoveAt(i);
        _positions.RemoveAt(i);
        OnGround = _grounds.Count > 0;
    }

    private void FixedUpdate()
    {
        Vector2 t = Vector2.zero;
        for (int i = _positions.Count - 1; i >= 0; i--)
        {
            if (!_grounds[i])
            {
                _grounds.RemoveAt(i);
                _positions.RemoveAt(i);
            }
            else
            {
                var delta = (Vector2) _grounds[i].transform.position - _positions[i];
                if (Mathf.Abs(delta.x) > t.x) t.x = delta.x;
                if (Mathf.Abs(delta.y) > t.y) t.y = delta.y;
                _positions[i] = _grounds[i].transform.position;
            }
        }

        if (_grounds.Count == 0) OnGround = false;
        _body.position += t;
    }
}
