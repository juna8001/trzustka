﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IShootable
{
    void OnShooted(Shooter shooter, Vector2 velocity);
}
