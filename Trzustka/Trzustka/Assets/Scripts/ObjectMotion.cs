﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMotion : MonoBehaviour
{

    [SerializeField]
    private LayerMask mask = int.MaxValue;
    [SerializeField]
    private float velocityMultipiler = 1f;
    [SerializeField]
    private bool disableColliders = false;
	[SerializeField]
	private float dynamicSpeed = 1f, dynamicRange = 1f;
    private new Camera camera;
    private Rigidbody2D target;
    private Vector2 offset;
    private RigidbodyType2D type;
    private Vector2 pastPointer;
    private Collider2D[] colliders;

    private void Awake()
    {
        camera = GetComponent<Camera>();
    }

    private void Update()
    {
        if (target == null)
        {
            if (Input.GetKeyDown(KeyCode.Mouse0))
            {
                Vector2 pointer = camera.ScreenToWorldPoint(Input.mousePosition);
                var hit = Physics2D.OverlapPoint(pointer, mask);
                if (hit != null)
                {
                    var body = hit.GetComponentInParent<Rigidbody2D>();
                    if (body != null)
                    {
                        Assign(body, pointer);
                    }
                }
            }
        }
        else
        {
            Vector2 pointer = camera.ScreenToWorldPoint(Input.mousePosition);
            Drag(pointer);
            if (Input.GetKeyUp(KeyCode.Mouse0))
            {
                Release(pointer);
            }
            else
            {
                pastPointer = pointer;
            }
        }
    }

    private void Assign(Rigidbody2D body, Vector2 pointer)
    {
        offset = body.position - pointer;
        type = body.bodyType;
        body.bodyType = type == RigidbodyType2D.Dynamic ? RigidbodyType2D.Dynamic : RigidbodyType2D.Kinematic;
        target = body;
        pastPointer = pointer;
        if (disableColliders)
        {
            colliders = body.GetComponentsInChildren<Collider2D>();
            foreach (Collider2D c in colliders)
            {
                c.enabled = false;
            }
        }
        else
        {
            colliders = null;
        }
    }

    private void Release(Vector2 pointer)
    {
        target.bodyType = type;
        target.velocity = ((pointer - pastPointer) / Time.deltaTime) * velocityMultipiler;
        target = null;

        if (disableColliders && colliders != null)
        {
            foreach (Collider2D c in colliders)
            {
                c.enabled = true;
            }
            colliders = null;
        }
    }

    private void Drag(Vector2 pointer)
    {
        if (type == RigidbodyType2D.Dynamic)
        {
			var pos = pointer + offset;
			target.velocity = Vector2.ClampMagnitude(pos - target.position, dynamicRange) * dynamicSpeed / dynamicRange;
        }
        else
        {
            target.position = pointer + offset;
        }
    }
}
