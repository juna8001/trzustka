﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderOnCursor : MonoBehaviour
{
    [SerializeField]
    private Camera _camera;

    private void Update()
    {
        transform.position = _camera.ScreenToWorldPoint(Input.mousePosition);
    }
}
