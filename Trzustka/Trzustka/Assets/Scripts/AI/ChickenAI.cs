﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class ChickenAI : MonoBehaviour
{
    [SerializeField] private float _width = 0.6f;
    [SerializeField] private Vector2 _attackDistanceRange = new Vector2(1, 1.5f);
    [SerializeField] private float _jumpAtttackChance = 0.8f;
    [SerializeField] private float _forceAttackDelay = 10f;

    private CharacterMovement _movement;
    private GroundChecker _groundChecker;
    private Attacker _attacker;
    private Shooter _shooter;
    private float _direction;
    private ContactFilter2D _groundFilter;
    private ContactFilter2D _enemyFilter;
    private ContactFilter2D _wallFilter;
    private RaycastHit2D[] _hits = new RaycastHit2D[1];
    private bool _shouldJump;
    private float _attackDistance = 1;
    private float _nextForcedAttackTime;

    protected void Start()
    {
        _nextForcedAttackTime = Time.time + _forceAttackDelay;
        _attackDistance = Random.Range(_attackDistanceRange.x, _attackDistanceRange.y);
        _shouldJump = Random.value < _jumpAtttackChance;
        _movement = GetComponent<CharacterMovement>();
        _attacker = GetComponent<Attacker>();
        _shooter = GetComponent<Shooter>();
        _groundChecker = GetComponentInChildren<GroundChecker>();
        _direction = Random.Range(0, 2) == 0 ? 1 : -1;
        _groundFilter = new ContactFilter2D
        {
            layerMask = LayerMask.GetMask("Ground", "Character", "PlayerCharacter"),
            useLayerMask = true
        };
        _enemyFilter = new ContactFilter2D
        {
            layerMask = LayerMask.GetMask("Character", "PlayerCharacter"),
            useLayerMask = true
        };
    }

    private void FixedUpdate()
    {
        if (Time.time >= _nextForcedAttackTime)
        {
            _actionCoroutine = StartCoroutine(JumpAttack());
        }

        var origin = transform.position + Vector3.up * 0.2f + Vector3.right * -_direction * _width;
        if (_actionCoroutine == null)
        {
            if (Physics2D.Raycast(origin, Vector2.right * -_direction, _enemyFilter, _hits, _attackDistance) != 0)
            {
                _direction = -_direction;
            }

            origin = transform.position + Vector3.up * 0.2f + Vector3.right * _direction * _width;
            if (Physics2D.Raycast(origin, Vector2.right * _direction, _enemyFilter, _hits, _attackDistance) != 0)
            {
                _actionCoroutine = StartCoroutine(_shouldJump ? JumpAttack() : Attack());
                _shouldJump = Random.value < _jumpAtttackChance;
                _attackDistance = Random.Range(_attackDistanceRange.x, _attackDistanceRange.y);
            }
        }

        origin = transform.position + Vector3.up * 0.2f + Vector3.right * _direction * _width;
        if (_groundChecker.OnGround)
        {
            if (Physics2D.Raycast(origin, Vector2.down, _groundFilter, _hits, 0.4f) == 0)
            {
                _direction = -_direction;
            }
        }

        if (Physics2D.Raycast(origin, Vector2.right * _direction, _groundFilter, _hits, 0.01f) != 0)
        {
            _direction = -_direction;
        }

        _movement.TryMove(_direction);
    }

    private Coroutine _actionCoroutine;

    private IEnumerator JumpAttack()
    {
        _movement.TryJump();
        yield return new WaitForSeconds(0.5f);
        _shooter.TryShoot(GetComponent<Rigidbody2D>().velocity);
        _actionCoroutine = null;
        _nextForcedAttackTime = Time.time + _forceAttackDelay;
    }

    private IEnumerator Attack()
    {
        _attacker.TryAttack();
        _actionCoroutine = null;
        yield break;
    }
}