﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class DuszekAI : MonoBehaviour
{
    [SerializeField]
    private float _maxSpeed;
    [SerializeField]
    private float _rotationSpeed;
    [SerializeField]
    private float _minimumRange = 8f;

    private Rigidbody2D _rigidbody;
    private Transform _player;
    private Vector2 _direction;
    private bool _isInitialized = false;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        PlayerController player = FindObjectOfType<PlayerController>();
        if(player != null)
        {
            Initialize(player.transform);
        }
        else
        {
            StartCoroutine(SearchForPlayer());
        }
    }

    private void Initialize(Transform player)
    {
        _player = player;
        _isInitialized = true;
    }

    private void Update()
    {
        if(!_isInitialized)
        {
            return;
        }
        if(_player == null)
        {
            _isInitialized = false;
            StartCoroutine(SearchForPlayer());
            return;
        }
        Vector3 distance = (_player.position - transform.position);
        if (distance.magnitude > _minimumRange)
        {
            return;
        }
        _direction = Vector2.Lerp(_direction, distance.normalized, _rotationSpeed * Time.deltaTime);
        _rigidbody.MovePosition((Vector2)transform.position + _direction * _maxSpeed * Time.deltaTime);
    }

    private IEnumerator SearchForPlayer()
    {
        PlayerController player = null;
        while (player == null)
        {
            _rigidbody.velocity = Vector2.zero;
            _rigidbody.angularVelocity = 0f;
            yield return new WaitForSeconds(1f);
            player = FindObjectOfType<PlayerController>();
        }
        Initialize(player.transform);
    }
}
