﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageOnHit : BaseTrigger
{
    [SerializeField] private int _damage = 1;
    [SerializeField] private bool _destroyOnHit;

    protected override void Trigger(Collider2D other)
    {
        var hp = other.GetComponentInParent<HealthOwner>();
        if(!hp) return;
        
        hp.HP -= _damage;
        if (_destroyOnHit)
        {
            Destroy(gameObject);
        }
    }
}
