﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class BaseTrigger : MonoBehaviour, IShootable
{
    protected abstract void Trigger(Collider2D other);

    private bool _active;
    private Shooter _shooter;

    protected void OnEnable()
    {
        StartCoroutine(Enable());
    }

    protected void OnDisable()
    {
        _active = false;
    }

    private IEnumerator Enable()
    {
        yield return null;
        _active = true;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var otherShooter = other.GetComponentInParent<Shooter>();
        if (!_active && (otherShooter == _shooter || otherShooter == GetComponentInParent<Shooter>())) return;
        Trigger(other);
    }

    public void Destroy()
    {
        Destroy(gameObject);
    }

    public void OnShooted(Shooter shooter, Vector2 velocity)
    {
        _shooter = shooter;
    }
}
