﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerOnLayerHit : BaseTrigger
{
	[Serializable] public class OnHitEvent : UnityEvent<Collider2D> { }
	
	[SerializeField] private LayerMask _mask;
	[SerializeField] private OnHitEvent OnHit;

	protected override void Trigger(Collider2D other)
	{
		if (((1<<other.gameObject.layer) & _mask) != 0)
		{
			OnHit.Invoke(other);
		}
	}
}
