﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateSlider : MonoBehaviour
{
    [SerializeField]
    private string key = "MusicVolume";

    protected void Start()
    {
        GetComponent<Slider>().value = PlayerPrefs.GetFloat(key, 1);
    }
}