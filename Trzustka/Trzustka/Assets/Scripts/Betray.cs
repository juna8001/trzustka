﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Betray : MonoBehaviour
{
	[SerializeField] private GameObject _replacer;

	private void OnTriggerEnter2D(Collider2D other)
	{
		var target = other.GetComponentInParent<PlayerController>();
		if (!target) return;
		Destroy(target);
		target.gameObject.AddComponent<ChickenAI>();
		target.gameObject.layer = LayerMask.NameToLayer("Character");
		//Instantiate(_replacer, target.transform.position, Quaternion.identity).transform.localScale = target.transform.localScale;
		
		//Destroy(target.gameObject);
		Destroy(gameObject);
	}
}
