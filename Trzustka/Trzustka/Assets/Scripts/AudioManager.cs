﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour {

	public static AudioManager instance;

	[SerializeField] private AudioMixerGroup _group;
	private AudioSource[] sources;
	private int index;

	private AudioSource Next {
		get{
			var s = sources[index];
			index = (index + 1) % sources.Length;
			return s;
		}
	}

	private void Awake() {
		instance = this;
		sources = new AudioSource[16];
		for(int i = 0; i < sources.Length; i++){
			sources[i] = gameObject.AddComponent<AudioSource>();
			sources[i].outputAudioMixerGroup = _group;
		}
	}

	public void Play(AudioClip clip){
		var s = Next;
		s.pitch = Random.Range(.9f, 1.1f);
		s.PlayOneShot(clip);
	}
}
