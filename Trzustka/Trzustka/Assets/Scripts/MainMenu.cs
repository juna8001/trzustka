﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private AudioMixer _mixer;
    [SerializeField] private GameObject _settings;
    [SerializeField] private GameObject _menu;
    [SerializeField] private Slider _musicSlider;
    [SerializeField] private Slider _soundSlider;
    
    protected void Start()
    {
        SetMusicVolume(PlayerPrefs.GetFloat("MusicVolume", 1));
        SetSoundVolume(PlayerPrefs.GetFloat("SoundVolume", 1));
        
        _settings.gameObject.SetActive(false);
        _menu.SetActive(true);
    }

    public void Play()
    {
        SceneManager.LoadScene(1);
    }

    public void OpenSettings()
    {
        _menu.SetActive(false);
        _settings.gameObject.SetActive(true);
    }

    public void OpenMenu()
    {
        _menu.SetActive(true);
        _settings.gameObject.SetActive(false);
    }

    public void SetMusicVolume(float value)
    {
        _mixer.SetFloat("MusicVolume", value == 0 ? -80 : Mathf.Log(value)*20);
        PlayerPrefs.SetFloat("MusicVolume", value);
    }

    public void SetSoundVolume(float value)
    {    
        _mixer.SetFloat("SoundVolume", value == 0 ? -80 : Mathf.Log(value)*20);
        PlayerPrefs.SetFloat("SoundVolume", value);
    }

    public void Exit()
    {
        Application.Quit();
#if UNITY_EDiTOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
    }
}
