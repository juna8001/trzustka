﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class HealthOwner : MonoBehaviour
{
    public UnityEvent onDeath, onRebirth;
    [SerializeField] private Slider hpBar;
    private int hp;
    [SerializeField] private int maxHP = 1;
    [SerializeField] private bool deathIsInevitable = true;
    private bool isDead = false;
    [SerializeField]
    private bool barFollowing = false;
    [SerializeField]
    private Vector3 barOffset;
    [SerializeField]
    private AudioClip deathSFX;

    public int HP
    {
        get { return hp; }
        set
        {
            if (hp == value) return;

            hp = Mathf.RoundToInt(value);
            if (hpBar) hpBar.value = value;
            if (hp <= 0)
            {
                isDead = true;
                onDeath.Invoke();
                if (hpBar && deathIsInevitable)
                {
                    hpBar.interactable = false;
                    hpBar.onValueChanged.RemoveAllListeners();
                }
                if (deathSFX != null)
                {
                    AudioManager.instance.Play(deathSFX);
                }
            }
            else if (isDead)
            {
                isDead = false;
                onRebirth.Invoke();
            }
        }
    }

    public int MaxHP
    {
        get { return maxHP; }
    }

    private void Awake()
    {
        hp = maxHP;
        if (hpBar)
        {
            hpBar.wholeNumbers = true;
            hpBar.value = hpBar.maxValue = hp = maxHP;
            hpBar.onValueChanged.AddListener(ChangeHp);
        }
    }

    private void ChangeHp(float v)
    {
        HP = Mathf.RoundToInt(v);
    }

    private void Update()
    {
        if (barFollowing)
        {
            hpBar.transform.position = Camera.main.WorldToScreenPoint(transform.position + barOffset);
        }
        else
        {
            var s = hpBar.transform.localScale;
            s.x = transform.localScale.x > 0f ? Mathf.Abs(s.x) : -Mathf.Abs(s.x);
            hpBar.transform.localScale = s;
        }
    }
}