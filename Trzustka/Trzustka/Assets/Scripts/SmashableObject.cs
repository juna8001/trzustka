﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SmashableObject : MonoBehaviour
{

    private static ContactPoint2D[] contacts = new ContactPoint2D[64];

	public float maxPenetration;
	public UnityEvent onPenetrated;

    private new Collider2D collider;

    private void Awake()
    {
        collider = GetComponent<Collider2D>();
    }

    private void Update()
    {
        var cCount = collider.GetContacts(contacts);
        var penetration = 0f;
        for (int i = 0; i < cCount; i++)
        {
            penetration = Mathf.Max(penetration, -contacts[i].separation);
        }
		if(penetration > maxPenetration){
			onPenetrated.Invoke();
		}
    }
}
