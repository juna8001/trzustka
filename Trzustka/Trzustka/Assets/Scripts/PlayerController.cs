﻿using System;
using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private CharacterMovement _movement;
    private Shooter _shooter;
    private Attacker _attacker;
    private Player _controller;

    protected void Start()
    {
        _movement = GetComponent<CharacterMovement>();
        _shooter = GetComponent<Shooter>();
        _attacker = GetComponent<Attacker>();
        _controller = ReInput.players.Players[0];
    }

    protected void FixedUpdate()
    {
        _movement.TryMove(_controller.GetAxis("Horizontal"));
        if(_controller.GetButtonDown("Jump")) _movement.TryJump();
        if(_controller.GetNegativeButtonDown("Vertical")) _shooter.TryShoot(GetComponent<Rigidbody2D>().velocity);
        if(_controller.GetButtonDown("Attack")) _attacker.TryAttack();
    }
}
