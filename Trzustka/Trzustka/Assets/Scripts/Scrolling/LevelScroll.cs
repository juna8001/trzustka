﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class LevelScroll : MonoBehaviour
{
    [SerializeField]
    private Bounds _levelBounds;
    [SerializeField]
    private Parallax.MainParallax _parallaxController;
    [SerializeField]
    private float _parallaxInteractionStrength;

    private Transform _player;

    private Camera _camera;
    private float _lastShift = 0f;
    private float _percent = 0f;
    private float _distance = 0f, _realMaxDistance;
    private bool _isInitialized;

    private void Awake()
    {
        _camera = GetComponent<Camera>();
        PlayerController player = FindObjectOfType<PlayerController>();
        if (player != null)
        {
            Initialize(player.transform);
        }
        else
        {
            StartCoroutine(SearchForPlayer());
        }
        _realMaxDistance = (_levelBounds.size.x - (_camera.orthographicSize * (16f / 9f) * 2f));
    }

    private void Initialize(Transform player)
    {
        _player = player;
        _isInitialized = true;
    }

    private void Update()
    {
        if (!_isInitialized)
        {
            return;
        }
        if (_player == null)
        {
            _isInitialized = false;
            StartCoroutine(SearchForPlayer());
            return;
        }
        float shift = (_player.position.x <= 0f) ? 0f : (_player.position.x >= _realMaxDistance) ? _realMaxDistance : _player.position.x;
        transform.position = new Vector3(shift, 0f, -10f);
        _parallaxController.MoveParallax(Vector3.right * (shift - _lastShift) * _parallaxInteractionStrength);
        _lastShift = shift;
    }

    private IEnumerator SearchForPlayer()
    {
        PlayerController player = null;
        while (player == null)
        {
            yield return new WaitForSeconds(1f);
            player = FindObjectOfType<PlayerController>();
        }
        Initialize(player.transform);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(0f, 1f, 1f, 0.3f);
        Gizmos.DrawCube(_levelBounds.center, _levelBounds.size);
    }
}
