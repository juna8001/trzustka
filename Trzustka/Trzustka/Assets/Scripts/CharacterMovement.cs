﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{
    private enum MovementType { Position, SetVelocity, AddVelocity }
    
    [SerializeField] private MovementType _movementType = MovementType.Position;
    [SerializeField] private float _speed = 5;
    [SerializeField] private float _jumpHeight = 2;
    [SerializeField] private float _jumpCooldown = 0.25f;
    [SerializeField] private int _maxInAirJumps = 1;
    [SerializeField] private Animator _animator;
    private bool _animating = false;

    public AudioClip jumpSFX;

    private int _jumps;
    private float _nextJumptime;
    private Rigidbody2D _body;
    private GroundChecker _groundChecker;
    private Attacker _attacker;

    protected void Start()
    {
        _body = GetComponent<Rigidbody2D>();
        _groundChecker = GetComponentInChildren<GroundChecker>();
        _attacker = GetComponent<Attacker>();
    }

    protected void FixedUpdate()
    {
        if (_groundChecker && _groundChecker.OnGround)
        {
            _jumps = _maxInAirJumps;
        }
    }

    public void TryMove(float direction)
    {
        if (direction == 0)
        {
            _animator.Play("Empty");
            _animating = false;
            return;
        }
        if (_attacker && _attacker.Active) return;

        if(!_animating)
        {
            _animator.Play("walk");
        }
        _animating = true;
        Move(direction);
    }

    public void Move(float direction)
    {
        var scale = transform.localScale;
        scale.x = Mathf.Abs(scale.x) * Mathf.Sign(direction);
        transform.localScale = scale;

        switch (_movementType)
        {
            case MovementType.Position:
            {
                _body.position += Time.fixedDeltaTime * Mathf.Sign(direction) * _speed * Vector2.right;
                break;
            }
            case MovementType.SetVelocity:
            {
                var v = _body.velocity;
                v.x = Mathf.Sign(direction) * _speed;
                _body.velocity = v;
                break;
            }
            case MovementType.AddVelocity:
            {
                var v = _body.velocity;
                v.x += Time.fixedDeltaTime * Mathf.Sign(direction) * _speed;
                _body.velocity = v;
                break;
            }
        }
    }

    public void TryJump()
    {
        if (_attacker && _attacker.Active) return;
            
        if (_groundChecker && _groundChecker.OnGround)
        {
            Jump();
        }
        else if(Time.time >= _nextJumptime && _jumps > 0)
        {
            _jumps--;
            Jump();
        }
    }

    public void Jump()
    {
        var v = _body.velocity;
        v.y = Mathf.Sqrt(-Physics2D.gravity.y*_jumpHeight*2*_body.gravityScale) / (1-Time.fixedDeltaTime*0.8f);
        _body.velocity = v;
        _nextJumptime = Time.time + _jumpCooldown;
        if(jumpSFX != null){
            AudioManager.instance.Play(jumpSFX);
        }
    }
}
