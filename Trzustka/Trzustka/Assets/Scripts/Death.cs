﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Death : MonoBehaviour
{
    [SerializeField] private float _length = 2;
    [SerializeField] private Vector2 _offsetRange = Vector2.one * 5;
    [SerializeField] private AnimationCurve _fallAngle;
    [SerializeField] private AnimationCurve _size;
    
    protected void Start()
    {
        GetComponent<HealthOwner>().onDeath.AddListener(Kill);
    }

    private void Kill()
    {
        if (_killCoroutine != null) return;
        _killCoroutine = StartCoroutine(KillCoroutine());
    }

    private Coroutine _killCoroutine;
    private IEnumerator KillCoroutine()
    {
        var body = GetComponent<Rigidbody2D>();
        if (body) body.simulated = false;
        foreach (var collider in GetComponentsInChildren<Collider2D>(true)) collider.enabled = false;
        foreach (var renderer in GetComponentsInChildren<SpriteRenderer>(true)) renderer.sortingOrder += 1000;
        Vector2 offset = Vector2.zero;
        offset.x = Random.Range(-_offsetRange.x, -_offsetRange.x);
        offset.y = Random.Range(-_offsetRange.y, -_offsetRange.y);
        var startPos = transform.position;
        var startScale = transform.localScale;
        float progress = 0;
        while (progress < 1)
        {
            yield return null;
            var progressOffset = _fallAngle.Evaluate(progress);
            transform.position = startPos + new Vector3(0, progressOffset) + Vector3.Lerp(Vector3.zero, offset, progress);
            transform.localScale = startScale * _size.Evaluate(progress);
            progress += Time.deltaTime / _length;
        }
        Destroy(gameObject);
    }
}
