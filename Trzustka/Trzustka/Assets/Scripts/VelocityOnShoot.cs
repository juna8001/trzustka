﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VelocityOnShoot : MonoBehaviour, IShootable
{
    public void OnShooted(Shooter shooter, Vector2 velocity)
    {
        var body = GetComponent<Rigidbody2D>();
        if (!body) return;
        body.velocity = velocity;
    }
}
