﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplodeDeath : MonoBehaviour
{
    [SerializeField] private float _lifetime = 0.5f;
    [SerializeField] private Transform _pivot;
    [SerializeField] private GameObject _explosion;
    
    protected void Start()
    {
        GetComponent<HealthOwner>().onDeath.AddListener(Kill);
    }

    private void Kill()
    {
        var t = transform;
        if(_pivot) t = _pivot;
        Destroy(Instantiate(_explosion, t.position, Quaternion.identity), _lifetime);        
        Destroy(gameObject, 0.01f);
    }
}
